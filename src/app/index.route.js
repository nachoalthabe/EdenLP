(function() {
  'use strict';

  angular
    .module('edenLp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/routes/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('lista', {
        url: '/lista',
        templateUrl: 'app/routes/lista/lista.html',
        controller: 'ListaController',
        controllerAs: 'lista'
      })
      .state('item', {
        url: '/item/:id',
        templateUrl: 'app/routes/item/item.html',
        controller: 'ItemController',
        controllerAs: 'item'
      })
      .state('llave', {
        url: '/llave',
        templateUrl: 'app/routes/llave/llave.html',
        controller: 'LlaveController',
        controllerAs: 'llave'
      })
      .state('editarDescripcion', {
        url: '/',
        templateUrl: 'app/routes/editarDescripcion/editarDescripcion.html',
        controller: 'EditarDescripcionController',
        controllerAs: 'editarDescripcion'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
