/* global malarkey:false, toastr:false, moment:false */
(function() {
  'use strict';

  var ids = {
    edenlp: 77726746,
    edenlp12: 89086749
  }

  angular
    .module('edenLp')
    .constant('SELLER_ID', ids.edenlp)
    .constant('APP_ID', 6360152884909900);

})();
