(function() {
  'use strict';

  angular
    .module('edenLp', ['angular-clipboard', 'angular-quill', 'ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ui.router', 'ui.bootstrap']);

})();
