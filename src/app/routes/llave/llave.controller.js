(function() {
  'use strict';

  angular
    .module('edenLp')
    .controller('LlaveController', LlaveController);

  /** @ngInject */
  function LlaveController(ML,Cargando) {
    var self = this;
    self.login = function() {
      Cargando.show();
      ML.login().then(function(data) {
        Cargando.hide();
        self.llave = ML.getToken();
      })
    }
    self.login();
  }
})();
