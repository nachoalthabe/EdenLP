(function() {
  'use strict';

  angular
    .module('edenLp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController(ML,Cargando,$state) {
    var self = this;
    self.login = function() {
      Cargando.show();
      ML.login().then(function() {
        Cargando.hide();
        $state.go('lista');
      })
    }
  }
})();
