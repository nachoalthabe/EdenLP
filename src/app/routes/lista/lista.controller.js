(function() {
  'use strict';

  angular
    .module('edenLp')
    .controller('ListaController', ListaController);

  /** @ngInject */
  function ListaController($scope, $state, ML, Cargando) {
    function cargarPagina(offset) {
      Cargando.show();
      ML.getItems(offset).then(function(data) {
        $scope.items = data[2].results;
        var paginasInfo = data[2].paging;
        $scope.paginador = {
          haySiguiente: (paginasInfo.offset + paginasInfo.limit) >= paginasInfo.total,
          hayAnterior: paginasInfo.offset == 0,
          pagina: Math.round(paginasInfo.offset / paginasInfo.limit) + 1,
          paginas: Math.round(paginasInfo.total / paginasInfo.limit) + 1,
          base: paginasInfo
        };
        console.log(data[2]);
        Cargando.hide();
      });
    }
    cargarPagina(0);
    $scope.anterior = function() {
      cargarPagina($scope.paginador.base.offset - $scope.paginador.base.limit);
    };

    $scope.siguiente = function() {
      cargarPagina($scope.paginador.base.offset + $scope.paginador.base.limit);
    };

    $scope.info = function(id) {
      $state.go('item', {
        id: id
      });
    }
  }
})();
