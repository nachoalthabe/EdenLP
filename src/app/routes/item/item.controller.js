(function() {
  'use strict';

  angular
    .module('edenLp')
    .controller('ItemController', ItemController);

  /** @ngInject */
  function ItemController($scope, $stateParams, ML, Cargando) {
    $scope.id = $stateParams.id;
    Cargando.show();
    ML.getItem($scope.id).then(function(data) {
      console.log(data[2]);
      ML.getDescription($scope.id).then(function(data) {
        Cargando.hide();
        $scope.descripcion = data[2].text;

        console.log('description', data[2]);
      });
    });
  };
})();
