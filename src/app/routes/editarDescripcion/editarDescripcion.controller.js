(function() {
  'use strict';

  angular
    .module('edenLp')
    .controller('EditarDescripcionController', EditarDescripcion);

  /** @ngInject */
  function EditarDescripcion($scope, $sce, Cargando, Plantilla) {
    $scope.descripcion = "";
    $scope.opciones = {
      theme: 'snow'
    };
    $scope.resultado = false;
    $scope.compilar = function() {
      $scope.resultado = false;
      Plantilla.compilar($scope.descripcion,0).then(function(resultado) {
        $scope.resultado = resultado;
      }, function() {

      });
    }
    $scope.success = function() {
      console.log('Copied!');
    };

    $scope.fail = function(err) {
      console.error('Error!', err);
    };

    $scope.getResultado = function() {
      if (!$scope.resultado) return "";
      return $sce.trustAsHtml($scope.resultado);
    }
  }
})();
