(function() {
  'use strict';

  angular
    .module('edenLp')
    .factory('Plantilla', Plantilla);

  /** @ngInject */
  function Plantilla($http, $q, $templateCache) {

    var src = {
      0: 'app/services/plantilla/plantilla.html'
    };

    function getTheme(theme) {
      console.log('tetas');
      return $http({
        method: 'GET',
        url: src[theme || 0],
        cache: $templateCache,
        responseType: ""
      })
      console.log('chocho');
    }

    function compilar(html,theme) {
      var description = angular.element(html);
      return $q(function(ok, fail) {
        getTheme(theme).then(function(response) {
          var vDom = angular.element(response.data);
          vDom.find('#edenlp_descripcion').append(description);
          ok(vDom.html());
        }, function() {
          fail(arguments);
        })
      });
    }

    return {
      compilar: compilar
    }
  }
})();
