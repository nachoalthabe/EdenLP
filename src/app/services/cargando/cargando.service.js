(function() {
  'use strict';

  angular
    .module('edenLp')
    .factory('Cargando', Cargando);

  angular
    .module('edenLp')
    .controller('ModalCargandoCtrl', function($scope, mensage) {
      $scope.mensage = mensage;
    });

  /** @ngInject */
  function Cargando($modal) {
    function show(mensage) {
      this.modal = $modal.open({
        animation: true,
        templateUrl: '/app/services/cargando/cargando.html',
        controller: 'ModalCargandoCtrl',
        resolve: {
          mensage: function() {
            return mensage;
          }
        }
      });
    }

    function hide() {
      this.modal.close();
    }

    return {
      show: show,
      hide: hide
    }
  }
})();
