(function() {
  'use strict';

  angular
    .module('edenLp')
    .factory('ML', ML);

  /** @ngInject */
  function ML($q, APP_ID, SELLER_ID) {
    var ready = false,
      waiting = [];
    MELI.init({
      client_id: APP_ID
    });

    function login() {
      return $q(function(resolve, reject) {
        MELI.login(function(data) {
          ready = true;
          resolve(data);
          waiting.forEach(function(cb) {
            cb();
          })
        });
      });
    }

    function getToken() {
      return MELI.getToken();
    }

    function onReady() {
      return $q(function(resolve, reject) {
        if (ready)
          resolve()
        else
          waiting.push(function() {
            resolve();
          });
      });
    }

    function getItems(offset) {
      return $q(function(resolve, reject) {
        MELI.get('/sites/MLA/search', {
          seller_id: SELLER_ID,
          offset: offset || 0
        }, function(data) {
          resolve(data);
        });
      });
    }

    function getItem(id) {
      return $q(function(resolve, reject) {
        MELI.get('/items/' + id, null, function(data) {
          resolve(data);
        });
      });
    }

    function getDescription(id) {
      return $q(function(resolve, reject) {
        MELI.get('/items/' + id + '/description', null, function(data) {
          resolve(data);
        });
      });
    }
    return {
      'login': login,
      'getToken': getToken,
      'onReady': onReady,
      'getItems': getItems,
      'getItem': getItem,
      'getDescription': getDescription
    };
  }

})();
