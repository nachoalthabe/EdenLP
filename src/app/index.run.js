(function() {
  'use strict';

  angular
    .module('edenLp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
