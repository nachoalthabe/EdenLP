var Crawler = require("crawler");
var url = require('url');
var request = require('request');
var fs = require('fs');
var db = require('diskdb');
var crypto = require('crypto');
db.connect('./db', ['items']);

var MS_url = 'http://edenlp.mercadoshops.com.ar/';

var c = new Crawler({
  maxConnections: 10
});

function getImages(images) {
  images.forEach(function(imageSrc) {
    if (imageSrc == 'http://static.mercadoshops.com/static/images/es_sin_foto_large.jpg')
      return;
    var src = url.parse(imageSrc),
      filename = __dirname + '/images' + src.pathname;
    fs.exists(filename, function(exists) {
      if (exists) return;
      request.head(imageSrc, function(err, res, body) {
        request(imageSrc).pipe(fs.createWriteStream(filename));
      });
    });
  })

}

function getItem(item, category) {
  console.log('getItem('+item+');');
  c.queue([{
    uri: MS_url + item,
    jQuery: true,

    // The global callback won't be called
    callback: function(error, result, $) {
      var images = $('#product_image ul.bxslider img').toArray().map(function(el) {
        return $(el).attr('src')
      });
      if (images.length == 0) {
        images = [$('#product_image img').attr('src')];
      }

      var title = $('h1.product_name').text(),
        hash = crypto.createHash('md5').update(title).digest('hex');

      var item_dupli = db.items.find({
        hash: hash
      });
      if (item_dupli.length > 0) return;

      var item = {
        hash: hash,
        title: title,
        category_id: category,
        price: $('#product_price span').text(),
        images: images,
        copy: item_dupli.length
      }

      var descPath = __dirname + '/descriptions/dump/' + hash + '_' + item.copy + '.html';
      fs.exists(descPath, function(exists) {
        if (exists) return;
        fs.writeFile(descPath, $('#product_description').html(), function(err) {
          if (err) {
            return console.log(err);
          }
        });
      });

      db.items.save(item);
      getImages(images);
    }
  }]);
}

function getCategoryPage(href, category) {
  console.log(MS_url + href);
  c.queue([{
    uri: MS_url + href,
    jQuery: true,

    // The global callback won't be called
    callback: function(error, result, $) {
      var next = $('.clearfix #pagination .next a').attr('href');
      console.log('next',next);
      if (next) {
        getCategoryPage(next, category);
      }
      $('.product_name a').each(function(i, _elm) {
        var elm = $(_elm);
        getItem(elm.attr('href').replace('/', ''), category);
      })
    }
  }]);
};

function getCategory(category) {
  //console.log('getCategory('+category+');');
  c.queue([{
    uri: MS_url + category,
    jQuery: true,

    // The global callback won't be called
    callback: function(error, result, $) {
      var next = $('.clearfix #pagination .next a').attr('href');
      if (next) {
        getCategoryPage(next, category);
      }
      $('.product_name a').each(function(i, _elm) {
        var elm = $(_elm);
        getItem(elm.attr('href').replace('/', ''), category);
      })
    }
  }]);
}

function getHome() {
  //console.log('getHome();');
  c.queue([{
    uri: MS_url,
    jQuery: true,

    // The global callback won't be called
    callback: function(error, result, $) {
      $('.ms-menu-list_item a').each(function(i, _elm) {
        var elm = $(_elm);
        getCategory(elm.attr('href').replace('/', ''));
      })
    }
  }]);
}

getHome();
