var db = require('diskdb'),
  fs = require('fs'),
  cheerio = require('cheerio');
db.connect('./db', ['items']);
var items = db.items.find();

items.forEach(function(item, i) {
  fs.readFile(__dirname + '/descriptions/dump/' + item.hash + '_' + item.copy + '.html', 'utf8', function(err, data) {
    if (err) {
      return console.log(err);
    }
    var descPath = __dirname + '/descriptions/base/' + item.hash + '_' + item.copy + '.html';

    //console.log(data);

    var $ = cheerio.load(data);
    var elm = $('#PRODUCT_EDENLP > table > tbody > tr > td');
    if (!elm.html())
      elm = $('div:first-child > table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td');
    if (!elm.html())
      elm = $('table:first-child > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td');
    if (!elm.html())
      elm = $('#PRODUCT_EDENLP');
    if (!elm.html())
      console.log('NoDesc ::', item.title, item.hash);
    fs.exists(descPath, function(exists) {
      if (exists) return;
      elm.find('*').removeAttr('style');
      fs.writeFile(descPath, elm.html(), function(err) {
        if (err) {
          return console.log(err);
        }
      });
    });
  });

})
