#!/usr/bin/env node

var program = require('commander'),
  getAll = require('./commands/get.js');

program
  .version('0.0.1')
  .command('get [id]')
  //.option('-a, --access_token [string]', 'LLave de acceso', false)
  //.option('-c, --collection [string]', 'Nombre de la coleccion', false)
  .action(function(id) {
    console.log('aca');
    getAll.do(program.access_token, program.collection, id);
  })
  .parse(process.argv);
