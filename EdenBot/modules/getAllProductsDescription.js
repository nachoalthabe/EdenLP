var Producto = require('../db').Producto('eden12c');
var Crawler = require("crawler");
var url = require('url');

var c = new Crawler({
  maxConnections: 10
});

var i = 1;

function getDescription(producto) {
  c.queue([{
    uri: 'https://api.mercadolibre.com/items/' + producto.ml_id + '/description',
    jQuery: false,
    // The global callback won't be called
    callback: function(err, result) {
      if (err) return console.log('Error recibiendo', producto.ml_id);
      var data = JSON.parse(result.body);
      producto.ml_descripcion = data.text;
      producto.save(function(err) {
        if (err) return console.log('Error Actualizando', producto.ml_info.title, err);
        console.log(i++);
      });
    }
  }]);
}

Producto.find({/* $where: "!this.ml_descripcion" */}, function(err, productos) {
  if (err) return console.log('Error Recuperando', err);
  productos.forEach(function(producto) {
    getDescription(producto);
  })
})
