var Producto = require('./db').Producto('eden12c');
var Crawler = require("crawler");
var cheerio = require('cheerio');
var fs = require('fs');
var request = require('request');

var c = new Crawler({
  maxConnections: 10
});

var i = 1;

fs.readdir('./images', function(err, files) {
  //files = ['564c24a3771ca3243ea78f37-2.jpeg'];
  files.forEach(function(filename) {
    var formData = {
      file: fs.createReadStream('./images/' + filename),
    };
    request.post({
      url: 'https://api.mercadolibre.com/pictures/?access_token=APP_USR-6360152884909900-120601-5d01db7573f8f2dc43b5ad4acce5098a__I_B__-89086749',
      formData: formData
    }, function optionalCallback(err, httpResponse, body) {
      if (err)
        return console.error(err,filename);
      var data = JSON.parse(body);
      Producto.findById(filename.replace(/-[0-9]*.[a-z-A-Z]*/g, ''),
        function(err, producto) {
          if (producto.imagenes.indexOf(data.id) >= 0)
            return console.log('Cargada');
          producto.imagenes.push(data.id);
          producto.save(function(err) {
            if (err)
              return console.log('Err', err);
            //console.log(data.id);
          })
        })
    });
  })
})
