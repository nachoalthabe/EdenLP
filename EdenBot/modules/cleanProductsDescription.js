var Producto = require('../db').Producto('eden_full');
var cheerio = require('cheerio');

var i = 1;

function cleanDescription(producto) {
  if (!producto.ml_descripcion) {
    return console.log('Vacio', producto.ml_id);
  }
  var $ = cheerio.load(producto.ml_descripcion);
  var elm = $('div:first-child > table > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td');
  if (!elm.html())
    elm = $('table > tbody > tr:nth-child(2) > td:nth-child(2)');
  if (!elm.html())
    elm = $('table:first-child > tbody > tr:nth-child(2) > td:nth-child(2) > table > tbody > tr > td');
  if (!elm.html())
    console.log('NoDesc ::', producto.ml_id);
  producto.descripcion = elm.html();
  producto.save(function(err) {
    if (err) return console.log('Error Actualizando', producto.ml_id, err);
    //console.log(i++, producto.ml_id);
  })
}

Producto.find({}, function(err, productos) {
  if (err) return console.log('Error Recuperando', err);
  productos.forEach(function(producto) {
    cleanDescription(producto);
  })
})
