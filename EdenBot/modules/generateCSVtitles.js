//Genera un csv con ID-ML y Titulo, para corregir los titulos mayores a 60 caracteres
//esto lo paso a un excel para que sean corregidos a maxConnections
var Producto = require('../db').Producto('eden12c');
var csv = require("fast-csv");
var fs = require('fs');

var csvStream = csv.format({headers: true}),
writableStream = fs.createWriteStream("my.csv");

writableStream.on("finish", function(){
  console.log("DONE!");
  process.exit();
});

csvStream.pipe(writableStream);

Producto.find({}).exec(function(err, productos) {
  if (err) return console.error(err);
  productos.forEach(function(producto) {
    producto.titulo = producto.ml_info.title = producto.ml_info.title.trim()+' 12c';
  })
  process.exit();
  //csvStream.end();
});
