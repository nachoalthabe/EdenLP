var Producto = require('../db').Producto('eden12c');
var Imagen = require('../db').Imagen('eden12c_imagenes');
var Crawler = require("crawler");
var cheerio = require('cheerio');
var fs = require('fs');
var url = require('url');
var querystring = require('querystring');
var request = require('request');

var c = new Crawler({
    maxConnections: 10
  }),
  i = 0;

function getPictureInfo(id, producto) {
  c.queue([{
    uri: 'https://api.mercadolibre.com/pictures/' + id,
    jQuery: false,
    // The global callback won't be called
    callback: function(error, result) {
      var data = JSON.parse(result.body);
      if (data.status == 404)
        return console.log('Err', id);
      if (producto.imagenes.indexOf(data.id) >= 0)
        return console.log('Cargada');
      producto.imagenes.push(data.id);
      producto.save(function(err) {
        if (err)
          return console.log('Err', err);
        console.log(id);
      })
    }
  }]);
};

function getPicture(src, producto) {
  c.queue([{
    uri: src,
    jQuery: false,
    // The global callback won't be called
    callback: function(error, result) {
      if(error)
        return console.error(error);
      if (result.headers['x-container']) {
        return true;
        var id = result.headers['x-container'] + '-' + result.headers['x-id'];
        getPictureInfo(id, producto);
      }
      var name = '../images/' + producto.id + '-' + (i++) + '.' + (result.headers['content-type'].split('/')[1]);
      request.head(src, function(err, res, body) {
        request(src).pipe(fs.createWriteStream(name)).on('close', function(){
          console.log('bajada',src);
        });
      });
    }
  }]);
};

Producto.find({}, function(err, productos) {
  if (err) return console.log('Error Recuperando', err);
  productos.forEach(function(producto) {
    producto.imagenes = [];
    var $ = cheerio.load(producto.descripcion);
    var imagesDom = $('img'),
      images = [];
    imagesDom.each(function(index, element) {
      var src = $(element).attr('src');
      if (src.indexOf('mlstatic.com') > -1) {
        return;
        src = url.parse(src).pathname.replace(/\-[A-Z].[a-z]{3}/g, '').replace('/', '');
        getPictureInfo(src, producto);
        //Ya estarian en ML
      }
      if (src.indexOf('mercadolibre.com') > -1) {
        return;
        getPicture(src, producto);
        //No tienen imagen
      }
      getPicture(src, producto);
      return;
    });
  });
  console.log('End!');
});
