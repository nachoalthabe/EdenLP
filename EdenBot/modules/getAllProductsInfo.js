var Producto = require('../db').Producto('eden12c');
var Crawler = require("crawler");
var url = require('url');

var c = new Crawler({
  maxConnections: 10
});

var i = 1;
var access_tocken = 'APP_USR-6360152884909900-040517-c9202a38f92c9f616ed5607b4f950ed4__F_H__-89086749';

function getInfo(producto) {
  c.queue([{
    uri: 'https://api.mercadolibre.com/items/' + producto.ml_id + '?access_token=' + access_tocken,
    jQuery: false,
    // The global callback won't be called
    callback: function(err, result) {
      if (err) return console.error('getInfo', producto.ml_id);
      var data = JSON.parse(result.body);
      producto.ml_info = data;
      producto.titulo = data.title;
      if (data.pictures) {
        producto.imagenes = data.pictures.map(function(picture) {
          return picture.id
        });
      } else {
        producto.imagenes = [];
      }
      producto.save(function(err) {
        console.log(i++, producto.ml_info.title);
      });
    }
  }]);
}

Producto.find({ titulo: { $exists: false } }, function(err, productos) {
  if (err) return console.error(err);
  productos.forEach(function(producto) {
    getInfo(producto);
  })
});
