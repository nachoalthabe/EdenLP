var Producto = require('../db').Producto('eden12c');
var Crawler = require("crawler");
var url = require('url');

var c = new Crawler({
  maxConnections: 10
});

var i = 1;
var access_tocken = 'APP_USR-6092-040417-3a992efc2f47e04b86096639326962c1__G_E__-77726746';

function getItems(offset) {
  c.queue([{
    uri: 'https://api.mercadolibre.com/users/77726746/items/search?access_token='+access_tocken+'&status=active&offset=' + offset + '&limit=100',
    jQuery: false,
    // The global callback won't be called
    callback: function(error, result) {
      var data = JSON.parse(result.body);
      var productos = data.results.map(function(id) {
        return {
          ml_id: id
        }
      });
      Producto.create(productos, function(err) {
        productos.forEach(function(item) {
          console.log(i++, item.ml_id);
        });
        if (err) return console.error('Error guardando', err);
      })
      if ((data.paging.offset + data.paging.limit) < data.paging.total)
        getItems(data.paging.offset + data.paging.limit);
    }
  }]);
}

getItems(0);
