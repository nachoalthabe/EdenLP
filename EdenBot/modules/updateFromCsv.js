//tomo lo generado por generateCSVtitle y lo actualizo en la base_price

var csv = require("fast-csv");
var Producto = require('../db').Producto('eden12c');

csv
  .fromPath("my.new.csv")
  .on("data", function(data) {
    Producto.findOne({
      ml_id: data[0]
    }).exec(function(err, producto) {
      console.log(data);
      if (err)
        console.log('No se pudo obtener', data[0]);
      if (!producto)
        console.log('No se pudo encontrar', data[0]);
      producto.titulo = data[1];
      producto.ml_info.title = data[1];
      producto.save(function(err) {
        if (err)
          console.log('No se pudo guardar', data[0]);
      })
    });
  })
  .on("end", function() {
    console.log("done");
  });
