var Producto = require('../db').Producto('eden12c');
var Crawler = require("crawler");
var url = require('url');

var c = new Crawler({
  maxConnections: 10
});

var i = 1;
var access_tocken = 'APP_USR-6360152884909900-040519-a79686649a2f6195b7cef3d04ed072ce__H_I__-89086749';

function addProduct(producto, callback) {
  var info = producto.ml_info;
  delete info.stop_time;
  delete info.original_price;
  delete info.seller;
  delete info.installments;
  delete info.id;
  delete info.thumbnail;
  delete info.permalink;
  delete info.address;
  delete info.sold_quantity;
  delete info.subtitle;
  delete info.initial_quantity;
  delete info.base_price;
  delete info.seller_contact;
  delete info.date_created;
  delete info.descriptions;
  delete info.geolocation;
  delete info.deal_ids;
  delete info.tags;
  delete info.international_delivery_mode;
  delete info.sub_status;
  delete info.parent_item_id;
  delete info.differential_pricing;
  delete info.secure_thumbnail;
  delete info.end_time;
  delete info.listing_source;
  delete info.seller_id;
  delete info.last_updated;
  delete info.start_time;
  delete info.warnings;
  info.seller_address = {
    id: parseInt(173713383)
  };
  info.listing_type_id = "gold_pro";
  info.price = parseInt(info.price) * 1.1;
  info.price = Math.round(info.price * 100) / 100;
c.queue([{
  uri: 'https://api.mercadolibre.com/items?access_token=' + access_tocken,
  method: 'POST',
  json: true,
  body: info,
  callback: function(err, result) {
    if (err) return console.error('Post', producto.id, err);
    var item = JSON.parse(result.body);
    if (item.status && item.status == 400) {
      console.log(item);
    }
    producto.ml_id = item.id;
    callback(producto);
  }
}]);
};
var i = 1;
Producto.find({}).exec(function(err, productos) {
  if (err) return console.error(err);
  productos.forEach(function(producto) {
    addProduct(producto, function() {
      console.log(i++);
    });
  })
});
