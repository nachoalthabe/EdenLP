exports.run = function(queue, producto, access_token, callback) {
  producto.imagenes.forEach(function(imagen) {
    queue.queue([{
      uri: 'https://api.mercadolibre.com/items/' + producto.ml_id + '/pictures?access_token=' + access_token,
      method: 'POST',
      json: true,
      body: {
        id: imagen
      },
      callback: function(error, result) {
        if (error) return console.error('updateStatus', producto.id, error);
        callback(producto);
      }
    }]);
  });
};
