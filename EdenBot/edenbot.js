#!/usr/bin/env node

var program = require('commander');

program
  .version('0.0.1')
  .option('-a, --access_token [string]', 'LLave de acceso', false)
  .option('-d, --address [string]', 'Id de la direccion', false)
  .option('-c, --collection [string]', 'Nombre de la coleccion', false)
  .parse(process.argv);

if (program.access_token == false) {
  console.error('Debes definir un AccessTocken -a');
  return process.exit(1);
}

if (program.collection == false) {
  console.error('Debes definir una Coleccion -c');
  return process.exit(1);
}

if (program.address == false) {
  console.error('Debes definir un Id de direccion -d');
  return process.exit(1);
}


var Producto = require('./db').Producto(program.collection),
  Crawler = require("crawler");

var Crear = require('./postInfo.js'),
  Estado = require('./updateStatus.js'),
  Foto = require('./updatePictures.js'),
  Descripcion = require('./updateDescription.js');


var crawler = new Crawler({
  maxConnections: 10
});

var productos = [];

Producto.find({
  //_id: '5665fa4c89cc6b6c2e72f7fc'
}, function(err, _productos) {
  productos = _productos;
  proximo();
});

function proximo() {
  if(productos.length == 0){
    productos.save(function(err){
      return console.log('Listo!',err);
    });
  }
  var producto = productos.pop();
  Crear.run(crawler, producto, program.access_token, program.address, function(producto) {
    Estado.run(crawler, producto, 'paused', program.access_token, function(producto) {
      Foto.run(crawler, producto, program.access_token, function(producto) {
        Descripcion.run(crawler, producto, program.access_token, function(producto) {
          Estado.run(crawler, producto, 'closed', program.access_token, function(producto) {
            console.log('Listo', producto.ml_id, producto.titulo);
            proximo();
          })
        })
      });
    });
  });
}
