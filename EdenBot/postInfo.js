var url = require('url');
//Nacho 172606793
//Eden 173713383

exports.run = function(queue, producto, access_token, address, callback) {
  var info = producto.ml_info;
  delete info.stop_time;
  delete info.original_price;
  delete info.seller;
  delete info.installments;
  delete info.id;
  delete info.thumbnail;
  delete info.permalink;
  delete info.address;
  delete info.sold_quantity;
  delete info.subtitle;
  delete info.initial_quantity;
  delete info.base_price;
  delete info.seller_contact;
  delete info.date_created;
  delete info.descriptions;
  delete info.geolocation;
  delete info.deal_ids;
  delete info.tags;
  delete info.international_delivery_mode;
  delete info.sub_status;
  delete info.parent_item_id;
  delete info.differential_pricing;
  delete info.secure_thumbnail;
  delete info.end_time;
  delete info.listing_source;
  delete info.seller_id;
  delete info.last_updated;
  delete info.start_time;
  info.seller_address = {
    id: parseInt(address)
  };
  info.status = 'paused';
  queue.queue([{
    uri: 'https://api.mercadolibre.com/items?access_token=' + access_token,
    method: 'POST',
    json: true,
    body: info,
    callback: function(err, result) {
      if (err) return console.error('Post', producto.id, err);
      var item = JSON.parse(result.body);
      producto.ml_id = item.id;
      callback(producto);
    }
  }]);
}
