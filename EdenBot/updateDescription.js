var cheerio = require('cheerio');
var fs = require('fs');

var theme = false;

fs.readFile(__dirname + '/theme.html', 'utf8', function(err, data) {
  theme = cheerio.load(data);
});

exports.run = function(queue, producto, access_token, callback) {
  theme('#edenlp_descripcion').html(producto.descripcion);
  var descripcion = theme.html();
  queue.queue([{
    uri: 'https://api.mercadolibre.com/items/' + producto.ml_id + '/description?access_token=' + access_token,
    method: 'PUT',
    json: true,
    body: {
      text: descripcion
    },
    callback: function(error, result) {
      if (error) return console.error('updateStatus', producto.id, error);
      callback(producto);
    }
  }]);
};
