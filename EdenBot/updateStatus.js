exports.run = function(queue, producto, status, access_token, callback) {
  queue.queue([{
    uri: 'https://api.mercadolibre.com/items/' + producto.ml_id + '?access_token=' + access_token,
    method: 'PUT',
    json: true,
    priority: 9,
    body: {
      status: status || 'closed'
    },
    callback: function(error, result) {
      if (error) return console.error('updateStatus', producto.id, error);
      callback(producto);
    }
  }]);
};
