var mongoose = require('mongoose');
var Schema = mongoose.Schema;
mongoose.connect('mongodb://localhost/edenlp');

exports.ProductoSchema = function(collection) {
  return new Schema({
    titulo: String,
    ml_id: {
      type: String,
      index: true,
      unique: true
    },
    ml_info: JSON,
    ml_descripcion: String,
    descripcion: String,
    imagenes: {
      type: Array,
      default: []
    },
  }, {
    collection: collection || 'productos'
  });
};

exports.ImagenSchema = function(collection) {
  return new Schema({
    producto: {
      type: Schema.Types.ObjectId,
      ref: 'Producto'
    },
    /*ml_id: {
      type: String,
      index: true,
      unique: true
    },*/
    ml_info: {
      type: JSON,
      default: false
    },
    src: {
      type: String,
      index: true,
      unique: true
    }
  }, {
    collection: collection || 'imagenes'
  });
};

exports.Imagen = function(collection) {
  return mongoose.model('Imagen', exports.ImagenSchema(collection));
};

exports.Producto = function(collection) {
  return mongoose.model('Producto', exports.ProductoSchema(collection));
};
